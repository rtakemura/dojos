package com.ciandt.poc.dojo1;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.List;

public class CountriesListActivity extends AppCompatActivity {

    private Spinner spinnerContinents;
    private ListView listViewCountry;
    private ProgressDialog progressDialog;
    private static final String SERVICE_URL = "https://query.yahooapis.com/v1/public/yql?q={0}&diagnostics=true&format=json";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_countries_list);
        spinnerContinents = (Spinner) findViewById(R.id.spinner_continents);
        listViewCountry = (ListView) findViewById(R.id.list_view_country);
        spinnerContinents.setSelection(0);
    }

    @Override
    protected void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void loadCountriesByContinent(String continentName) {
        if (progressDialog == null) {
            progressDialog = ProgressDialog.show(this,
                    "Carregando",
                    "Por favor, espere");
        }
        AsyncTask<String,Void,List<String>> asyncTask = new AsyncTask<String, Void, List<String>>() {
            @Override
            protected List<String> doInBackground(String... params) {
                return null;
            }
            @Override
            public void onPostExecute(List<String> result) {
            }
        };
        asyncTask.execute();
    }
}
