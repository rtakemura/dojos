var express = require('express');
var bodyParser = require('body-parser');
var swig = require('swig');
var app = express();
app.use(bodyParser.json());

app.listen(8080, function() {
    console.log('Start server in port 8080');
});
