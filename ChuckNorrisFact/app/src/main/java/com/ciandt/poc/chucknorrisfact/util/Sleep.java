package com.ciandt.poc.chucknorrisfact.util;

import android.util.Log;

/**
 * Created by ricardot on 07/09/16.
 */
public final class Sleep {

    public static void waiting(long milisseconds) {
        try {
            Thread.sleep(milisseconds);
        } catch (Exception e) {
            Log.e(Sleep.class.getName(), e.getMessage(), e);
        }
    }
}
