package com.ciandt.poc.chucknorrisfact.service;

import android.util.Log;

import com.ciandt.poc.chucknorrisfact.util.StreamUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ricardot on 07/09/16.
 */
public class ChuckNorrisFactService {

    public static final int DEFAULT_FACTS_AMOUNT = 20;

    public List<String> getFacts(int size) {
        List<String> facts = new ArrayList<>();
        HttpURLConnection conn = null;
        try {
            URL url = new URL("http://api.icndb.com/jokes/random/" + size);
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoInput(true);
            conn.connect();
            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                String json = StreamUtils.getResponseBody(conn);
                JSONObject root = new JSONObject(json);
                JSONArray values = root.getJSONArray("value");
                for (int i = 0; i < values.length(); i++) {
                    facts.add(values.getJSONObject(i).getString("joke"));
                }
            }
        } catch (Exception e) {
            Log.e(ChuckNorrisFactService.class.getName(), e.getMessage(), e);
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
        return facts;
    }


}
