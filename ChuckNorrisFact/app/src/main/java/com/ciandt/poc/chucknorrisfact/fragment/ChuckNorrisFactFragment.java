package com.ciandt.poc.chucknorrisfact.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ciandt.poc.chucknorrisfact.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * Use the {@link ChuckNorrisFactFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChuckNorrisFactFragment extends Fragment {

    // the fragment initialization parameters, e.g. FACT_ARG
    private static final String FACT_ARG = "fact";

    private String mFact;

    public ChuckNorrisFactFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param fact Parameter 1.
     * @return A new instance of fragment ChuckNorrisFactFragment.
     */
    public static ChuckNorrisFactFragment newInstance(String fact) {
        ChuckNorrisFactFragment fragment = new ChuckNorrisFactFragment();
        Bundle args = new Bundle();
        args.putString(FACT_ARG, fact);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mFact = getArguments().getString(FACT_ARG);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragmentView = inflater.inflate(R.layout.fragment_chuck_norris_fact, container, false);
        TextView factTextView = (TextView) fragmentView.findViewById(R.id.factTextView);
        factTextView.setText(mFact);
        factTextView.setContentDescription(mFact);
        return fragmentView;
    }
}
