package com.ciandt.poc.chucknorrisfact.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.ciandt.poc.chucknorrisfact.fragment.ChuckNorrisFactFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ricardot on 07/09/16.
 */
public class ChuckNorrisFactPagerAdapter extends FragmentPagerAdapter {

    private List<String> mFacts = new ArrayList<>();

    public ChuckNorrisFactPagerAdapter(FragmentManager fragmentManager, List<String> facts) {
        super(fragmentManager);
        mFacts = facts;
    }

    @Override
    public Fragment getItem(int position) {
        return ChuckNorrisFactFragment.newInstance(mFacts.get(position));
    }

    @Override
    public int getCount() {
        return mFacts.size();
    }
}
