package com.ciandt.poc.chucknorrisfact.activity;

import android.app.ProgressDialog;
import android.net.Uri;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.ciandt.poc.chucknorrisfact.adapter.ChuckNorrisFactPagerAdapter;
import com.ciandt.poc.chucknorrisfact.service.ChuckNorrisFactService;
import com.ciandt.poc.chucknorrisfact.R;
import com.ciandt.poc.chucknorrisfact.fragment.ChuckNorrisFactFragment;
import com.ciandt.poc.chucknorrisfact.task.ChuckNorrisFactAsyncTask;

import java.util.List;

public class ChuckNorrisFactMainActivity extends AppCompatActivity implements ChuckNorrisFactAsyncTask.ChuckNorrisFactListener {

    private ProgressDialog mProgressDialog;
    private ViewPager mChuckNorrisFactViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chuck_norris_fact_main);
        mChuckNorrisFactViewPager = (ViewPager) findViewById(R.id.chuckNorrisFactViewPager);
    }

    @Override
    public void onResume() {
        super.onResume();
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(getString(R.string.loading));
        mProgressDialog.show();
        final ChuckNorrisFactAsyncTask asyncTask = new ChuckNorrisFactAsyncTask(this);
        asyncTask.execute(ChuckNorrisFactService.DEFAULT_FACTS_AMOUNT);
    }

    @Override
    public void onLoaded(List<String> facts) {
        final ChuckNorrisFactPagerAdapter pagerAdapter = new ChuckNorrisFactPagerAdapter(getSupportFragmentManager(), facts);
        mChuckNorrisFactViewPager.setAdapter(pagerAdapter);
        mProgressDialog.dismiss();
    }
}
