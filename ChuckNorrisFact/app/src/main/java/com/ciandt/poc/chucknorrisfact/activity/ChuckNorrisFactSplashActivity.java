package com.ciandt.poc.chucknorrisfact.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.ciandt.poc.chucknorrisfact.R;
import com.ciandt.poc.chucknorrisfact.util.Sleep;

public class ChuckNorrisFactSplashActivity extends AppCompatActivity {

    public static final int WAITING_TIME = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chuck_norris_fact_splash);
        final Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Sleep.waiting(WAITING_TIME);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        nextScreen();
                    }
                });
            }
        });
        thread.start();
    }


    private void nextScreen() {
        Intent intent = new Intent(this, ChuckNorrisFactMainActivity.class);
        startActivity(intent);
        this.finish();
    }
}
