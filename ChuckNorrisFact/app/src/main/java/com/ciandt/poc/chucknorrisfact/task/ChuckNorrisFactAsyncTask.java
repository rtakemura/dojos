package com.ciandt.poc.chucknorrisfact.task;

import android.os.AsyncTask;

import com.ciandt.poc.chucknorrisfact.service.ChuckNorrisFactService;

import java.util.List;

/**
 * Created by ricardot on 07/09/16.
 */
public class ChuckNorrisFactAsyncTask extends AsyncTask<Integer, Integer, List<String>> {

    public interface ChuckNorrisFactListener {
        void onLoaded(List<String> facts);
    }

    private ChuckNorrisFactListener mListener;
    private ChuckNorrisFactService mService;

    public ChuckNorrisFactAsyncTask(ChuckNorrisFactListener listener) {
        mListener = listener;
        mService = new ChuckNorrisFactService();
    }

    @Override
    protected void onPostExecute(List<String> s) {
        super.onPostExecute(s);
        if (mListener != null) {
            mListener.onLoaded(s);
        }
    }

    @Override
    protected List<String> doInBackground(Integer... params) {
        int amount = ChuckNorrisFactService.DEFAULT_FACTS_AMOUNT;
        if (params != null && params.length > 0) {
            amount = params[0];
        }
        return mService.getFacts(amount);
    }
}
