package com.ciandt.poc.chucknorrisfact.util;

import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;

/**
 * Created by ricardot on 07/09/16.
 */
public final class StreamUtils {

    public static final String NEW_LINE = "\n";

    public static String getResponseBody(HttpURLConnection conn) {
        BufferedReader bufferedReader = null;
        StringBuilder builder = new StringBuilder();
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                builder.append(line).append(NEW_LINE);
            }
        } catch (Exception e) {
            Log.e(StreamUtils.class.getName(), e.getMessage(), e);
        } finally {
            close(bufferedReader);
        }
        return builder.toString();
    }

    public static void close(Reader reader) {
        try {
            if (reader != null) {
                reader.close();
            }
        } catch (Exception e) {
            Log.e(StreamUtils.class.getName(), e.getMessage(), e);
        }
    }
}
