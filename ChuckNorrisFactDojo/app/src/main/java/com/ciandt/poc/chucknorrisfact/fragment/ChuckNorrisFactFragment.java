package com.ciandt.poc.chucknorrisfact.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ciandt.poc.chucknorrisfact.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * Use the {@link ChuckNorrisFactFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChuckNorrisFactFragment extends Fragment {

    static final String FACT_ARG = "fact";
    private String mFact;

    /*
     Crie a seguinte propriedade estática e constante:
        - FACT_ARG: objeto da class String com o valor "fact"
     Crie a seguinte propriedades privada:
        - mFact: objeto da class String
     */

    public ChuckNorrisFactFragment() {
        super();
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param fact Parameter 1.
     * @return A new instance of fragment ChuckNorrisFactFragment.
     */
    public static ChuckNorrisFactFragment newInstance(String fact) {
        ChuckNorrisFactFragment cFragment = new ChuckNorrisFactFragment();
        Bundle bundle = new Bundle();
        bundle.putString(FACT_ARG, fact);
        cFragment.setArguments(bundle);
        return cFragment;
        /*
         - Crie um objeto da classe ChuckNorrisFactFragment e instancie este objeto com o construtor default
         - Crie um objeto da classe Bundle e instancie este objeto com o construtor default
         - Use o método putString do objeto da classe Bundle passando como primeiro argumento a constante
           FACT_ARG e o segundo argumento a variavel fact (passado neste método como paramêtro)
         - Use o método setArguments do objeto da classe ChuckNorrisFactFragment e passe o objeto da classe Bundle
           como paramêtro
         - Retorne o objeto da classe ChuckNorrisFactFragment
         - Remova o return abaixo
         */

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        String ret="";
        if (bundle != null){
           ret = bundle.getString(FACT_ARG);
        }
        mFact = ret;
        /*

         - Chame o método getArguments e guarde seu retorno em um objeto da classe Bundle
         - Verifique se este objeto não é nulo
         - Se não for, use o método getString do objeto da classe Bundle passando a constante FACT_ARG como paramêtro
         - Guarde o valor retornado na propriedade mFact da classe
         */
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chuck_norris_fact,container,false);
        TextView factTextView = (TextView) view.findViewById(R.id.factTextView);
        factTextView.setText(mFact);
        factTextView.setContentDescription(mFact);
        /*
         - Chame o método inflate do objeto inflater (objeto da classe LayoutInflater e passado como paramêtro neste método)
           e passe como paramêtros:
           - O id do layout do Fragmento (constante R.layout.<nome_do_layout_do_fragmento>)
           - O objeto container (objeto da classe ViewGroup e passado como paramêtro neste método)
           - O valor constante false
         - Guarde o retorno do método em um objeto da classe View
         - Chame o método findViewById do objeto da classe View retornado e
           passe o id do TextView declarado do xml (R.id.<id_do_TextView>)
         - Guarde o retorno do método acima em um objeto da classe TextView (faça cast, se necessário)
         - Use o método setText do objeto da classe TextView e passe como parametro a propriedade mFact
         - Use o método setContentDescription do objeto da classe TextView e passe como parametro a propriedade mFact
         - Retorne objeto da classe View declarado anteriormente
         - Remova o return abaixo
         */
        return view;
    }
}
