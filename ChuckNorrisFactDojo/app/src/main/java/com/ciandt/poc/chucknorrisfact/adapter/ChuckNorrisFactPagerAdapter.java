package com.ciandt.poc.chucknorrisfact.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.ciandt.poc.chucknorrisfact.fragment.ChuckNorrisFactFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ricardot on 07/09/16.
 */
public class ChuckNorrisFactPagerAdapter extends FragmentPagerAdapter {

    private List<String> mFacts;
    /*
     Crie a seguinte propriedades privada:
        - mFacts: objeto da interface List<String>
     */
    public ChuckNorrisFactPagerAdapter(FragmentManager fragmentManager, List<String> dados) {
        super (fragmentManager);
        mFacts = dados;
    }

    /*
     Crie um construtor da classe que recebe como paramêtro um objeto da classe FragmentManager e um outro objeto da interface List<String>:
        - Passe o primeiro parametro para o construtor da classe pai
        - Guarde o segundo parametro na propriedade mFacts
        - Remova o construtor abaixo
     */

    @Override
    public Fragment getItem(int position) {

        String fact = mFacts.get(position);
        return  ChuckNorrisFactFragment.newInstance(fact);
        /*

         - Chame o método get da propriedade mFacts e guarde seu valor em um objeto da classe String
         - Chame o método estático newInstance da classe ChuckNorrisFactFragment
           e passe o objeto da classe String retornado anteriormente
         - Guarde seu retorno em um objeto da classe ChuckNorrisFactFragment
         - Retorne este objeto
         - Remova o return abaixo
         */

    }

    @Override
    public int getCount() {

        int size = mFacts.size();
        /*

         - Chame o método size da propriedade mFacts e guarde seu valor em um variavel do tipo inteiro
         - Retorne esta variavel
         - Remova o return abaixo
         */
        return size;
    }
}
