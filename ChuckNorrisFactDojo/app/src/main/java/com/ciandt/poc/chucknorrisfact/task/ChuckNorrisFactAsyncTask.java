package com.ciandt.poc.chucknorrisfact.task;

import android.os.AsyncTask;

import com.ciandt.poc.chucknorrisfact.service.ChuckNorrisFactService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ricardot on 07/09/16.
 */
public class ChuckNorrisFactAsyncTask extends AsyncTask<Integer, Void, List<String>> {


    public interface ChuckNorrisFactListener {

        public void onLoaded(List<String> list);
    }

    private ChuckNorrisFactListener mListener;
    private ChuckNorrisFactService mService;

    /*
     Crie as seguintes propriedades privadas:
        - mListener: objeto da interface ChuckNorrisFactListener
        - mService: objeto da classe ChuckNorrisFactService
     */

    public ChuckNorrisFactAsyncTask(ChuckNorrisFactListener chuckNorrisFactListener){
        this.mListener =  chuckNorrisFactListener;

        this.mService = new ChuckNorrisFactService();
    }
    /*
     Crie um construtor da classe que recebe como paramêtro um objeto da interface ChuckNorrisFactListener:
        - Guarde este parametro na propriedade mListener
        - Instancie a propriedade mService com seu construtor default (classe ChuckNorrisFactService)
     */

    @Override
    protected List<String> doInBackground(Integer... params) {
        List<String> facts = new ArrayList<>();

        Integer factsAmount = ChuckNorrisFactService.DEFAULT_FACTS_AMOUNT;

        if (params != null) {
            factsAmount = params[0];
        }
        facts = mService.getFacts(factsAmount);
        /*
         - Crie uma variavel do tipo inteiro que recebe a constante ChuckNorrisFactService.DEFAULT_FACTS_AMOUNT
         - Verifique se o array de objetos params não é null e se ele tem, pelo menos, um item
         - Se isso for verdadeiro, guarde o primeiro objeto na variavel declarada anteriormente
         -
         Fora da condição, use o método getFacts da propriedade mService e
           guarde seu retorno no objeto da interface List<String> declarado acima
         */
        return facts;
    }

    @Override
    protected void onPostExecute(List<String> facts) {
        super.onPostExecute(facts);

        if (mListener  != null) {
            mListener.onLoaded(facts);
        }
        /*

         - Verifique se a propriedade mListener esta instanciada
         - Se isso for verdadeiro, chame o método onLoaded desta propriedade e passe como parametro
            o objeto facts da interface List<String>
         */
    }


}
