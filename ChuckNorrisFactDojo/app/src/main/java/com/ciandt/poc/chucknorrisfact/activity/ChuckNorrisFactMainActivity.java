package com.ciandt.poc.chucknorrisfact.activity;

import android.app.ProgressDialog;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.ciandt.poc.chucknorrisfact.adapter.ChuckNorrisFactPagerAdapter;
import com.ciandt.poc.chucknorrisfact.service.ChuckNorrisFactService;
import com.ciandt.poc.chucknorrisfact.R;
import com.ciandt.poc.chucknorrisfact.task.ChuckNorrisFactAsyncTask;

import java.util.List;


/*

 Implementar a interface ChuckNorrisFactAsyncTask.ChuckNorrisFactListener nesta classe
 */
public class ChuckNorrisFactMainActivity extends  AppCompatActivity implements ChuckNorrisFactAsyncTask.ChuckNorrisFactListener  {

    private ProgressDialog mProgressDialog;
    private android.support.v4.view.ViewPager mChuckNorrisFactViewPager;

    /*
     Crie as seguintes propriedades privadas:
     - Uma propriedade chamada mProgressDialog da classe ProgressDialog
     - Uma propriedade chamada mChuckNorrisFactViewPager da classe android.support.v4.view.ViewPager
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chuck_norris_fact_main);

        mChuckNorrisFactViewPager = (android.support.v4.view.ViewPager) findViewById(R.id.chuckNOrrisFactViewPager);

        /*
         - Use o método findViewById e passe o id do android.support.v4.view.ViewPager
         que você definiu no activity_chuck_norris_fact_main.
            Observação: Para obter este id use a seguinte instrução:
            R.id.<nome_do_id>
         - Guarde o retorno (e faça cast, caso necessário) na propriedade mChuckNorrisFactViewPager
         */
    }

    @Override
    public void onResume() {
        super.onResume();

        mProgressDialog = new ProgressDialog(this);
        String mensagem = getString(R.string.loading);
        mProgressDialog.setMessage(mensagem);
        mProgressDialog.show();
        /*
         - Instancie a propriedade mProgressDialog com o construtor da classe ProgressDialog,
         passando em seu parâmetro a própria Activity (this)
         - Obtenha a mensagem de "carregando" através do método getString (use R.string.<nome>) e
         guarde em um objeto String
         - Coloque esta mensagem através do método setMessage da propriedade mProgressDialog
         - Exiba esta caixa de mensagem através do método show da propriedade mProgressDialog
         */
        ChuckNorrisFactAsyncTask asyncTask = new ChuckNorrisFactAsyncTask(this);
        asyncTask.execute(ChuckNorrisFactService.DEFAULT_FACTS_AMOUNT);

        /*
         - Crie um objeto da classe ChuckNorrisFactAsyncTask e em seu construtor, passe
         o objeto "this" referente a esta classe
         - Chame o método execute do objeto da classe ChuckNorrisFactAsyncTask e passe
         como paramêtro a constante ChuckNorrisFactService.DEFAULT_FACTS_AMOUNT
         */
    }

    @Override
    public void onLoaded(List<String> list) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        ChuckNorrisFactPagerAdapter pagerAdapter = new ChuckNorrisFactPagerAdapter(fragmentManager, list);
        mChuckNorrisFactViewPager.setAdapter(pagerAdapter);
        mProgressDialog.dismiss();
    }

    /*

     Implementar o método onLoaded da interface ChuckNorrisFactAsyncTask.ChuckNorrisFactListener
     - Guarde o objeto da classe android.support.v4.app.FragmentManager
       que vem do método getSupportFragmentManager (chame este método)
     - Crie um objeto da classe ChuckNorrisFactPagerAdapter
       e passe em seu construtor o retorno do método anterior
       e o paramêtro que foi passado neste método
     - Use o método setAdapter da propriedade mChuckNorrisFactViewPager
       passando o objeto da classe ChuckNorrisFactPagerAdapter criado anteriormente
     - Use o método dismiss da propriedade mProgressDialog
     */
}
