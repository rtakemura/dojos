package com.ciandt.poc.chucknorrisfact.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.ciandt.poc.chucknorrisfact.R;
import com.ciandt.poc.chucknorrisfact.util.Sleep;

public class ChuckNorrisFactSplashActivity extends AppCompatActivity {

    public static final int WAITING_TIME = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chuck_norris_fact_splash);
        final Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Sleep.waiting(WAITING_TIME);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        nextScreen();
                    }
                });
            }
        });
        thread.start();
    }


    private void nextScreen() {
        /*
        Chame a próxima tela (ChuckNorrisFactMainActivity) e termine esta Activity
        (ao voltar para esta tela, a aplicação será fechada).
        - Crie um objeto da classe Intent e passe, em seu construtor, o próprio objeto (this)
        e a class da próxima activity (que neste caso é a ChuckNorrisFactMainActivity);
        - Chame a função startActivity e passe o objeto da classe Intent
        - Chame a função finish para terminar esta Activity
         */
        Intent intent = new Intent(this, ChuckNorrisFactMainActivity.class);
        startActivity(intent);
        finish();
    }
}
