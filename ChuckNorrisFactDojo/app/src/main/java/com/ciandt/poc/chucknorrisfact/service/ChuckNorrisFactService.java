package com.ciandt.poc.chucknorrisfact.service;

import android.util.Log;

import com.ciandt.poc.chucknorrisfact.util.StreamUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ricardot on 07/09/16.
 */
public class ChuckNorrisFactService {

    public static final int DEFAULT_FACTS_AMOUNT = 20;

    public List<String> getFacts(int size) {
        List<String> facts = new ArrayList<>();
        HttpURLConnection conn = null;
        try {
            /*
             - Crie um objeto da classe URL e passe a seguinte string
               (adicione da variavel "size" passada por parametro no final dela):
               "http://api.icndb.com/jokes/random/" + size
             - Abra a conexão com o método openConnection do objeto da classe URL e
               receba seu retorno no objeto da classe HttpURLConnection declarado acima
             - Use o método setDoInput do objeto da classe HttpURLConnection e passe o valor true para ele
             - Use o método connect do objeto da classe HttpURLConnection
             - Verifique se a API respondeu certo através do retorno do método getResponseCode
               do objeto da classe HttpURLConnection (verifique se este retorno é igual a constante
               HttpURLConnection.HTTP_OK)
             - Se for igual, obtenha seus dados através do método estático getResponseBody da classe StreamUtils
               (guarde este retorno em um objeto da classe String)
             */
            URL url = new URL("http://api.icndb.com/jokes/random/" + size);
            conn = (HttpURLConnection)url.openConnection();
            conn.setDoInput(true);
            conn.connect();
            int retorno = conn.getResponseCode();
            String streamUtils = "";
            if(retorno == HttpURLConnection.HTTP_OK){
              streamUtils = StreamUtils.getResponseBody(conn);
            }


            JSONObject jsonObject =  new JSONObject(streamUtils);


            JSONArray jarray = jsonObject.getJSONArray("value");
            for(int i=0;i< jarray.length();i++){
                JSONObject jobject = jarray.getJSONObject(i);
                String joke = jobject.getString("joke");
                facts.add(joke);
            }
            /*
             - Crie um objeto da classe JSONObject e passe, em seu construtor,
               os dados guardados no objeto da classe String
             - Use o método getJSONArray passando como paramêtro o nome do nó que
               representa uma lista (neste caso, a constante "value").
               Por exemplo:
               {"nomes": ["Adriana", "Lucas"]}
               <JSONObject>.getJSONArray("nomes");
             - Guarde o retorno desta função em um objeto da classe JSONArray
             - Percorra este objeto, utilizando um for (utilize o método length para saber o tamanho do JSONArray)
             - Para obter cada item, utilize a função getJSONObject passando o indice do item que gostaria de recuperar
             - Guarde o retorno desta função em um objeto da classe JSONObject
             - Obtenha o valor da propriedade, através do método getString do objeto da classe JSONObject anterior,
               passando o nome do nó que o possue (neste caso, "joke")
             - Adicione o retorno do método anterior ao objeto da interface List<String> declarado (facts)
             */
        } catch (Exception e) {
            Log.e(ChuckNorrisFactService.class.getName(), e.getMessage(), e);
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
            /*
             - Verifique se o objeto da classe HttpURLConnection (conn) esta instanciado
             - Se estiver, chame o método disconnect para fechar a conexão
             */
        }
        return facts;
    }


}
