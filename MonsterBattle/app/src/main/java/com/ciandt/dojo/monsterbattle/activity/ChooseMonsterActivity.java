package com.ciandt.dojo.monsterbattle.activity;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;

import com.ciandt.dojo.monsterbattle.R;
import com.ciandt.dojo.monsterbattle.adapter.MonsterFragmentPagerAdapter;
import com.ciandt.dojo.monsterbattle.model.Monster;
import com.ciandt.dojo.monsterbattle.service.MonsterBattleSoundService;
import com.ciandt.dojo.monsterbattle.util.Constants;

public class ChooseMonsterActivity extends FragmentActivity {

    private Button monsterButton;
    private ViewPager monsterViewPager;
    private MonsterFragmentPagerAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_monster);
        pagerAdapter = new MonsterFragmentPagerAdapter(getSupportFragmentManager());
        monsterButton = (Button) findViewById(R.id.monsterButton);
        monsterViewPager = (ViewPager) findViewById(R.id.monsterViewPager);
        monsterViewPager.setAdapter(pagerAdapter);
        monsterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChooseMonsterActivity.this, BattleMonstersActivity.class);
                Monster monster = pagerAdapter.getValue(monsterViewPager.getCurrentItem());
                intent.putExtra(Constants.MONSTER_PARAM, monster);
                startActivity(intent);
            }
        });
    }

    private void startBackgroundSound() {
        Intent intent = new Intent(this, MonsterBattleSoundService.class);
        intent.setAction(MonsterBattleSoundService.PLAY);
        intent.putExtra(MonsterBattleSoundService.SOUND_PARAM, MonsterBattleSoundService.SELECT_SOUND);
        startService(intent);
    }

    private void pauseBackgroundSound() {
        Intent intent = new Intent(this, MonsterBattleSoundService.class);
        intent.setAction(MonsterBattleSoundService.PAUSE);
        intent.putExtra(MonsterBattleSoundService.SOUND_PARAM, MonsterBattleSoundService.SELECT_SOUND);
        startService(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        pauseBackgroundSound();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startBackgroundSound();
    }
}
