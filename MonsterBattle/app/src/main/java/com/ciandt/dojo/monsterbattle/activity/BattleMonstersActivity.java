package com.ciandt.dojo.monsterbattle.activity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.ciandt.dojo.monsterbattle.R;
import com.ciandt.dojo.monsterbattle.model.Monster;
import com.ciandt.dojo.monsterbattle.service.MonsterBattleSoundService;
import com.ciandt.dojo.monsterbattle.util.BattleMonsterUtils;
import com.ciandt.dojo.monsterbattle.util.Constants;

import org.w3c.dom.Text;

public class BattleMonstersActivity extends AppCompatActivity {

    private ImageButton attackButton;
    private ImageButton defenseButton;
    private ImageButton magicButton;
    private ImageView myMonsterImageView;
    private ImageView myMonsterScratchImageView;
    private ImageView otherMonsterImageView;
    private ImageView otherMonsterScratchImageView;
    private TextView myMonsterTextView;
    private TextView myMonsterLifeTextView;
    private TextView myMonsterMagicTextView;
    private TextView otherMonsterTextView;
    private TextView otherMonsterLifeTextView;
    private TextView otherMonsterMagicTextView;
    private Monster myMonster;
    private Monster otherMonster;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_battle_monsters);
        otherMonster = Monster.monsters().get((int) Math.round((Math.random() * 2)));
        myMonster = (Monster) getIntent().getSerializableExtra(Constants.MONSTER_PARAM);
        myMonsterImageView = (ImageView) findViewById(R.id.myMonsterImageView);
        myMonsterImageView.setImageResource(myMonster.resource);
        myMonsterScratchImageView = (ImageView) findViewById(R.id.myMonsterScratchImageView);
        myMonsterScratchImageView.setVisibility(View.INVISIBLE);
        myMonsterTextView = (TextView) findViewById(R.id.myMonsterTextView);
        myMonsterTextView.setText(myMonster.name);
        myMonsterLifeTextView = (TextView) findViewById(R.id.myMonsterLifeTextView);
        myMonsterLifeTextView.setText(myMonster.life.toString());
        myMonsterMagicTextView = (TextView) findViewById(R.id.myMonsterMagicTextView);
        myMonsterMagicTextView.setText(myMonster.magic.toString());
        otherMonsterImageView = (ImageView) findViewById(R.id.otherMonsterImageView);
        otherMonsterImageView.setImageResource(otherMonster.resource);
        otherMonsterScratchImageView = (ImageView) findViewById(R.id.otherMonsterScratchImageView);
        otherMonsterScratchImageView.setVisibility(View.INVISIBLE);
        otherMonsterTextView = (TextView) findViewById(R.id.otherMonsterTextView);
        otherMonsterTextView.setText(otherMonster.name);
        otherMonsterLifeTextView = (TextView) findViewById(R.id.otherMonsterLifeTextView);
        otherMonsterLifeTextView.setText(otherMonster.life.toString());
        otherMonsterMagicTextView = (TextView) findViewById(R.id.otherMonsterMagicTextView);
        otherMonsterMagicTextView.setText(otherMonster.magic.toString());
        attackButton = (ImageButton) findViewById(R.id.attackButton);
        attackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attack();
            }
        });
        defenseButton = (ImageButton) findViewById(R.id.defenseButton);
        magicButton = (ImageButton) findViewById(R.id.magicButton);

    }

    private void attack() {
        otherMonsterScratchImageView.setVisibility(View.VISIBLE);
        otherMonsterScratchImageView.invalidate();
        myMonster.attack(otherMonster);
        playAttackSound();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        BattleMonsterUtils.sleep();
                        otherMonsterLifeTextView.setText(otherMonster.life.toString());
                        otherMonsterScratchImageView.setVisibility(View.INVISIBLE);
                        otherMonsterScratchImageView.invalidate();
                    }
                });
            }
        });
        thread.start();
    }



    private void playAttackSound() {
        Intent intent = new Intent(this, MonsterBattleSoundService.class);
        intent.setAction(MonsterBattleSoundService.PLAY);
        intent.putExtra(MonsterBattleSoundService.SOUND_PARAM, MonsterBattleSoundService.ATTACK_SOUND);
        startService(intent);
    }

    private void playMagicSound() {
        Intent intent = new Intent(this, MonsterBattleSoundService.class);
        intent.setAction(MonsterBattleSoundService.PLAY);
        intent.putExtra(MonsterBattleSoundService.SOUND_PARAM, MonsterBattleSoundService.MAGIC_SOUND);
        startService(intent);
    }

    private void startBackgroundSound() {
        Intent intent = new Intent(this, MonsterBattleSoundService.class);
        intent.setAction(MonsterBattleSoundService.PLAY);
        intent.putExtra(MonsterBattleSoundService.SOUND_PARAM, MonsterBattleSoundService.BATTLE_SOUND);
        startService(intent);
    }

    private void pauseBackgroundSound() {
        Intent intent = new Intent(this, MonsterBattleSoundService.class);
        intent.setAction(MonsterBattleSoundService.PAUSE);
        intent.putExtra(MonsterBattleSoundService.SOUND_PARAM, MonsterBattleSoundService.BATTLE_SOUND);
        startService(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        pauseBackgroundSound();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startBackgroundSound();
    }
}
