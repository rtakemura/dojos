package com.ciandt.dojo.monsterbattle.util;

/**
 * Created by ricardot on 07/09/16.
 */
public class BattleMonsterUtils {

    public static final int TIME = 2000;

    public static void sleep() {
        try {
            Thread.sleep(TIME);
        } catch (Exception e) {}
    }
}
