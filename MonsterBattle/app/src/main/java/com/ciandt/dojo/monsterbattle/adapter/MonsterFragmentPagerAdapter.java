package com.ciandt.dojo.monsterbattle.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.ciandt.dojo.monsterbattle.fragment.MonsterCardFragment;
import com.ciandt.dojo.monsterbattle.model.Monster;

import java.util.List;

/**
 * Created by ricardotakemura on 06/09/16.
 */

public class MonsterFragmentPagerAdapter extends FragmentPagerAdapter {

    private List<Monster> monsters;

    public MonsterFragmentPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
        monsters = Monster.monsters();
    }

    @Override
    public Fragment getItem(int position) {
        Monster monster = monsters.get(position);
        MonsterCardFragment monsterCardFragment = MonsterCardFragment.newInstance(monster.name,
                monster.resource);
        return monsterCardFragment;
    }

    public Monster getValue(int position) {
        return monsters.get(position);
    }

    @Override
    public int getCount() {
        return monsters.size();
    }
}
