package com.ciandt.dojo.monsterbattle.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ricardotakemura on 03/09/16.
 */

public class Magic implements Serializable, Comparable<Magic> {

    public enum Type {
        AIR,
        WATER,
        FIRE
    }
    public Integer id;
    public String name;
    public Type type;
    public Integer power;

    @Override
    public int compareTo(Magic o) {
        return id.compareTo(o.id);
    }

    public static List<Magic> magics() {
        List<Magic> magics = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            Magic magic = new Magic();
            magic.id = i + 1;
            switch (i) {
                case 0:
                    magic.name = "FURACÃO";
                    magic.type = Type.AIR;
                    magic.power = 3;
                    break;
                case 1:
                    magic.name = "CURA";
                    magic.type = Type.WATER;
                    magic.power = -5;
                    break;
                case 2:
                    magic.name = "FOGO";
                    magic.type = Type.FIRE;
                    magic.power = 5;
            }
            magics.add(magic);
        }
        return magics;
    }
}
