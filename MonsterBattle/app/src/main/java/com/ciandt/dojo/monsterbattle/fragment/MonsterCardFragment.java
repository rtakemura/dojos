package com.ciandt.dojo.monsterbattle.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ciandt.dojo.monsterbattle.R;

public class MonsterCardFragment extends Fragment {

    private static final String ARG_NAME = "name";
    private static final String ARG_IMAGE_RESOURCE = "imageResource";

    private String name;
    private Integer imageResource;


    public MonsterCardFragment() {
        super();
    }

    public static MonsterCardFragment newInstance(String name, Integer imageResource) {
        MonsterCardFragment fragment = new MonsterCardFragment();
        Bundle args = new Bundle();
        args.putString(ARG_NAME, name);
        args.putInt(ARG_IMAGE_RESOURCE, imageResource);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            name = getArguments().getString(ARG_NAME);
            imageResource = getArguments().getInt(ARG_IMAGE_RESOURCE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragmentView = inflater.inflate(R.layout.fragment_monster_card, container, false);
        ImageView monsterImageView = (ImageView) fragmentView.findViewById(R.id.monsterImageView);
        monsterImageView.setImageResource(imageResource);
        TextView monsterNameTextView = (TextView) fragmentView.findViewById(R.id.monsterNameTextView);
        monsterNameTextView.setText(name);
        return fragmentView;
    }
}
