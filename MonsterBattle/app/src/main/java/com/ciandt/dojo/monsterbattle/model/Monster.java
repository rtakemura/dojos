package com.ciandt.dojo.monsterbattle.model;

import android.util.Log;

import com.ciandt.dojo.monsterbattle.R;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ricardotakemura on 03/09/16.
 */

public class Monster implements Serializable, Comparable<Monster> {

    public Integer id;
    public String name;
    public Integer resource;
    public Integer life;
    public Integer magic;
    public Integer attack;
    public Integer defense;
    public Boolean modeDefense = false;
    public List<Magic> magics = new ArrayList<>();

    public void attack(Monster monster) {
        int damage = (int) Math.round(Math.random() * this.attack);
        Log.d(Monster.class.getName(), "Attack: " + damage);
        damage -= monster.modeDefense ? defense : (int) Math.round(Math.random() * monster.defense);
        Log.d(Monster.class.getName(), "Damage: " + damage);
        monster.life = monster.life > damage ? (damage > 0 ? monster.life - damage : monster.life) : 0;
        Log.d(Monster.class.getName(), "Life: " + monster.life);
        monster.modeDefense = false;
    }

    public void magic(Magic magic, Monster monster) {
        if (this.magics.contains(magic) && this.magic > magic.power) {
            this.magic -= magic.power;
            int damage =  (int) Math.round(magic.power - (monster.modeDefense ? monster.defense : Math.random() * monster.defense));
            monster.life = monster.life > damage ? monster.life - damage : 0;
            monster.modeDefense = false;
        }
    }

    @Override
    public int compareTo(Monster o) {
        return id.compareTo(o.id);
    }

    public static List<Monster> monsters() {
        List<Monster> monsters = new ArrayList<>();
        List<Magic> magics = Magic.magics();
        for (int i = 0; i < 3; i++) {
            Monster monster = new Monster();
            monster.id = i + 1;
            switch (i) {
                case 0:
                    monster.name = "Diabinho";
                    monster.resource = R.drawable.monster1;
                    monster.life = 20;
                    monster.magic = 15;
                    monster.attack = 3;
                    monster.defense = 3;
                    monster.magics = new ArrayList<>();
                    monster.magics.addAll(magics.subList(0,2));
                    break;
                case 1:
                    monster.name = "Bruxilda";
                    monster.resource = R.drawable.monster2;
                    monster.life = 10;
                    monster.magic = 20;
                    monster.attack = 1;
                    monster.defense = 5;
                    monster.magics = magics;
                    break;
                case 2:
                    monster.name = "Abobrinha";
                    monster.resource = R.drawable.monster3;
                    monster.life = 15;
                    monster.magic = 10;
                    monster.attack = 5;
                    monster.defense = 1;
                    monster.magics = new ArrayList<>();
                    monster.magics.addAll(magics.subList(0,1));
            }
            monsters.add(monster);
        }
        return monsters;
    }

}
