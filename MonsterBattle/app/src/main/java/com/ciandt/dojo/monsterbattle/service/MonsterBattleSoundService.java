package com.ciandt.dojo.monsterbattle.service;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.ciandt.dojo.monsterbattle.R;

/**
 * Created by ricardot on 07/09/16.
 */
public class MonsterBattleSoundService extends IntentService {

    private MediaPlayer mpBattle;
    private MediaPlayer mpSelect;
    private MediaPlayer mpMagic;
    private MediaPlayer mpAttack;

    public static final String PLAY = "PLAY";
    public static final String STOP = "STOP";
    public static final String PAUSE = "PAUSE";
    public static final String SOUND_PARAM = "SOUND_PARAM";
    public static final int BATTLE_SOUND = 2;
    public static final int SELECT_SOUND = 1;
    public static final int MAGIC_SOUND = 3;
    public static final int ATTACK_SOUND = 4;

    public MonsterBattleSoundService() {
        super("MonsterBattleSoundService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
    }

    @Override
    public void onCreate() {
        mpBattle = MediaPlayer.create(this, R.raw.battle);
        mpBattle.setLooping(true);
        mpSelect = MediaPlayer.create(this, R.raw.choose);
        mpSelect.setLooping(true);
        mpAttack = MediaPlayer.create(this, R.raw.attack);
        mpMagic = MediaPlayer.create(this, R.raw.magic);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mpBattle.release();
        mpSelect.release();
        mpAttack.release();
        mpMagic.release();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private MediaPlayer getMediaPlayer(int code) {
        switch (code) {
            case BATTLE_SOUND: return mpBattle;
            case SELECT_SOUND: return mpSelect;
            case ATTACK_SOUND: return mpAttack;
            case MAGIC_SOUND: return mpMagic;
        }
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            MediaPlayer mediaPlayer = getMediaPlayer(intent.getIntExtra(SOUND_PARAM, 0));
            if (mediaPlayer != null) {
                if (STOP.equals(intent.getAction())) {
                    mediaPlayer.stop();
                } else if (PLAY.equals(intent.getAction())) {
                    mediaPlayer.start();
                } else if (PAUSE.equals(intent.getAction())) {
                    mediaPlayer.pause();
                }
            }
        }
        return START_STICKY;

    }




}
