package com.ciandt.poc.countryinfo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

import java.net.URLEncoder;
import java.text.MessageFormat;

public class CountryDetailsActivity extends AppCompatActivity {

    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_country_details);

            webView = (WebView) findViewById(R.id.webView);
            webView.getSettings().setJavaScriptEnabled(true);
            String url = MessageFormat.format("https://en.m.wikipedia.org/wiki/{0}", getIntent().getStringExtra("country").replace(' ','_'));
            webView.loadUrl(url);
        } catch (Exception e) {}
    }
}
