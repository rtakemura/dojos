package com.ciandt.poc.countryinfo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

public class CountriesListActivity extends AppCompatActivity {

    private Spinner continents;
    private ListView countryList;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_countries_list);

        continents = (Spinner) findViewById(R.id.continents);
        countryList = (ListView) findViewById(R.id.countryList);

        continents.setSelection(0);

        initEvents();
        loadCountriesByContinent("Africa");

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void loadCountriesByContinent(String nomeContinente){
        if(progressDialog == null){
            progressDialog = ProgressDialog.show(CountriesListActivity.this, "Carregando","Aguarde...");
        }

        AsyncTask<String, Void, List<String>> asyncTask = new AsyncTask<String, Void, List<String>>() {
            @Override
            protected List<String> doInBackground(String... params) {
                List<String> ret = new ArrayList<>();
                try {
                    String query = MessageFormat.format("select name from geo.countries where place=''{0}''", params[0]);
                    String q = URLEncoder.encode(query, "utf-8");
                    String urlStr = "https://query.yahooapis.com/v1/public/yql?q=" + q + "&diagnostics=true&format=json";
                    URL url = new URL(urlStr);
                    Log.i("CountriesListActivity", "URL: " + url);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    BufferedReader reader = new BufferedReader( new InputStreamReader(conn.getInputStream()));
                    String line = "";
                    String json = "";
                    while ((line = reader.readLine()) != null) {
                        json += line;
                    }
                    Log.i("CountriesListActivity", "JSON: " + json);
                    JSONObject jsonObject = new JSONObject(json);
                    JSONArray places = jsonObject.getJSONObject("query").getJSONObject("results").getJSONArray("place");
                    for (int i = 0; i < places.length(); i++){
                        ret.add(places.getJSONObject(i).getString("name"));
                    }
                } catch (Exception e) {
                }
                return ret;
            }

            @Override
            public void onPostExecute(List<String> result) {
                ArrayAdapter<String> arrayAdpter = new ArrayAdapter<String>(CountriesListActivity.this, android.R.layout.simple_list_item_1);
                arrayAdpter.addAll(result);
                countryList.setAdapter(arrayAdpter);
                if (progressDialog != null){
                    progressDialog.dismiss();
                    progressDialog = null;
                }

            }

        };



        asyncTask.execute(nomeContinente);
    }

    private void initEvents(){
        continents.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String continent = (String) continents.getAdapter().getItem(position);
                loadCountriesByContinent(continent);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        countryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String country = (String) countryList.getAdapter().getItem(position);
                Intent intent = new Intent(CountriesListActivity.this, CountryDetailsActivity.class);
                intent.putExtra("country", country);
                startActivity(intent);
            }
        });
    }
}
