package com.ciandt.gloriaresponde.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.ciandt.gloriaresponde.R;


public class SplashScreenActivity extends AppCompatActivity {

    private static final int AUTO_HIDE_DELAY_MILLIS = 2500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showMainScreen();
            }
        });
    }

    private void showMainScreen() {
        final Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(AUTO_HIDE_DELAY_MILLIS);
                } catch (Exception e) {
                    Log.e(getClass().getName(), e.getMessage(), e);
                }
                Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
                startActivity(intent);
                SplashScreenActivity.this.finish();
            }
        });
        thread.start();
    }
}
