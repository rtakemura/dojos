package com.ciandt.gloriaresponde.asynctask;

import android.os.AsyncTask;
import android.util.Log;

import com.ciandt.gloriaresponde.constants.Constants;
import com.ciandt.gloriaresponde.listener.OnMessageListener;
import com.ciandt.gloriaresponde.model.Message;
import com.ciandt.gloriaresponde.util.MessageStreamUtil;

import org.json.JSONArray;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ricardotakemura on 06/03/16.
 */
public class ListMessagesAsyncTask extends AsyncTask<Void, Integer, List<Message>> {

    private OnMessageListener onMessageListener;


    public OnMessageListener getOnMessageListener() {
        return onMessageListener;
    }

    public void setOnMessageListener(OnMessageListener onMessageListener) {
        this.onMessageListener = onMessageListener;
    }

    @Override
    protected List<Message> doInBackground(Void... params) {
        List<Message> messages = new ArrayList<>();

        messages = MessageStreamUtil.callListMessage();

        /*TODO 4
         - Obtenha a lista de mensagens do servidor usando o método estático MessageStreamUtil.callListMessage
         - Guarde em um objeto da classe List<Message>
         - Verifique se não é nulo
         - Se não for nulo, faça que o objeto messages recebe este novo valor
         - Retorne o objeto messages
         */
        return messages;
    }

    @Override
    protected void onPostExecute(List<Message> messages) {
        super.onPostExecute(messages);

        if(onMessageListener !=null){
            onMessageListener.onLoadMessages(messages);
        }
        /*TODO 5
          - Verifique se o objeto onMessageListener esta instanciado (onMessageListener != null)
          - Se estiver chame o método onLoadMessages deste objeto (onMessageListener.onLoadMessages)
          passando como parametro o objeto messages (List<Message>)
         */
    }

}
