package com.ciandt.gloriaresponde.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ciandt.gloriaresponde.R;
import com.ciandt.gloriaresponde.model.Message;

import java.util.List;

/**
 * Created by ricardotakemura on 06/03/16.
 */
public class MessageListAdapter extends ArrayAdapter<Message> {

    public MessageListAdapter(final Context context, final List<Message> messages) {
        super(context, R.layout.item_message_list_view, messages);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final View view = createOrUpdateView(convertView);
        final Message message = getItem(position);
        final ImageView imgMessage = (ImageView) view.findViewById(R.id.imgMessage);
        if (Message.UserType.GLORIA == message.getUserType()) {
            imgMessage.setImageResource(R.drawable.gloria);
        } else {
            imgMessage.setImageResource(R.drawable.default_user);
        }
        final TextView txtMessage = (TextView) view.findViewById(R.id.txtMessage);
        txtMessage.setText(message.getMessage());
        return view;
    }

    private View createOrUpdateView(View convertView) {
        //Obtem o item view da lista
        View view = convertView;
        //Se não existir ainda, cria-se um novo usando o serviço LayoutInflater
        if (view == null) {
            final LayoutInflater layoutInflater = (LayoutInflater)
                    getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.item_message_list_view, null);
        }
        return view;
    }
}
