package com.ciandt.gloriaresponde.listener;

import com.ciandt.gloriaresponde.model.Message;

import java.util.List;

/**
 * Created by ricardotakemura on 06/03/16.
 */
public interface OnMessageListener {

    void onLoadMessages(List<Message> messages);
    void onSendMessage(Message response);
}