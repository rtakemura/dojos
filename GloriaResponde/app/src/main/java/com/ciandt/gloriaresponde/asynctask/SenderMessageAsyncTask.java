package com.ciandt.gloriaresponde.asynctask;

import android.os.AsyncTask;
import android.util.Log;

import com.ciandt.gloriaresponde.constants.Constants;
import com.ciandt.gloriaresponde.listener.OnMessageListener;
import com.ciandt.gloriaresponde.model.Message;
import com.ciandt.gloriaresponde.util.MessageStreamUtil;

import org.json.JSONObject;

import java.net.URL;
import java.util.List;

/**
 * Created by ricardotakemura on 06/03/16.
 */
public class SenderMessageAsyncTask extends AsyncTask<Message, Integer, Message> {

    private OnMessageListener onMessageListener;


    public OnMessageListener getOnMessageListener() {
        return onMessageListener;
    }

    public void setOnMessageListener(OnMessageListener onMessageListener) {
        this.onMessageListener = onMessageListener;
    }

    @Override
    protected Message doInBackground(Message... params) {
        Message message = new Message(Message.UserType.GLORIA, Message.MESSAGE_DEFAULT);

        if(params != null && params.length>0){
           message = MessageStreamUtil.callSendMessage(params[0]);
        }
        /*TODO 6
         - Verifique se pelo menos um parametro foi passado, ou seja,
         se params != null e params.length > 0
         - Em caso afirmativo, chame o método estático callSendMessage da classe MessageStreamUtil
         (MessageStreamUtil.callSendMessage) passando como parametro o primeiro item do array params
         (MessageStreamUtil.callSendMessage(params[0])
         - Coloque o retorno deste método no objeto message
         - Retorne o objeto message
         */
        return message;
    }

    @Override
    protected void onPostExecute(Message message) {
        super.onPostExecute(message);
        if (onMessageListener != null) {
            onMessageListener.onSendMessage(message);
        }
        /*TODO 7
          - Verifique se o objeto onMessageListener esta instanciado (onMessageListener != null)
          - Se estiver chame o método onSendMessage deste objeto (onMessageListener.onSendMessage)
          passando como parametro o objeto message (Message)
         */
    }
}
