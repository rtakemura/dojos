package com.ciandt.poc.pais;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Thread thread = new Thread(new Runnable() {
            public void run() {
                try { Thread.sleep(2000); } catch (Exception e) {}
                runOnUiThread(new Runnable() {
                    public void run() {
                        Intent intent = new Intent(SplashActivity.this, CountriesListActivity.class);
                        startActivity(intent);
                        SplashActivity.this.finish();
                    }});
            }});
        thread.start();
    }
}
