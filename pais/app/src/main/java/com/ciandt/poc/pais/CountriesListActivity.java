package com.ciandt.poc.pais;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

public class CountriesListActivity extends AppCompatActivity {

    private Spinner mContinentSpinner;
    private ListView mCountryListView;
    private ProgressDialog mProgressDialog;
    private static final String SERVICE_URL = "https://query.yahooapis.com/v1/public/yql?q={0}&diagnostics=true&format=json";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_countries_list);
        mContinentSpinner = (Spinner) findViewById(R.id.continent_spinner);
        mCountryListView = (ListView) findViewById(R.id.country_list_view);
        mContinentSpinner.setSelection(0);
        initEvents();
        loadCountriesByContinent("Africa");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        super.finish();
    }

    public void loadCountriesByContinent(String continent) {
        if (mProgressDialog == null) {
            mProgressDialog = ProgressDialog.show(this, "Carregando", "Por favor, espere");
        }
        AsyncTask<String,Void,List<String>> asyncTask = new AsyncTask<String, Void, List<String>>() {
            @Override
            protected List<String> doInBackground(String... params) {
                List<String> result = new ArrayList<>();
                try {
                    String query = URLEncoder.encode(MessageFormat.format("select name from geo.countries where place=''{0}''", params[0]), "UTF-8");
                    String serviceUrl =  MessageFormat.format(SERVICE_URL, query);
                    URL url = new URL(serviceUrl);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.connect();
                    if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                        String line = null;
                        String content = "";
                        while ((line = reader.readLine()) != null) {
                            content += line;
                        }
                        JSONObject root = new JSONObject(content);
                        JSONArray array = root.getJSONObject("query").getJSONObject("results").getJSONArray("place");
                        for(int i = 0; i < array.length(); i++) {
                            result.add(array.getJSONObject(i).getString("name"));
                        }
                    }
                } catch (Exception e) {
                    Log.e(CountriesListActivity.class.getName(), e.getMessage(), e);
                }
                return result;
            }

            @Override
            public void onPostExecute(List<String> result) {
                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(CountriesListActivity.this, android.R.layout.simple_list_item_1);
                arrayAdapter.addAll(result);
                mCountryListView.setAdapter(arrayAdapter);
                if (mProgressDialog != null) {
                    mProgressDialog.dismiss();
                }
                mProgressDialog = null;
            }
        };
        asyncTask.execute(continent);
    }


    public void initEvents() {
        mContinentSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String continent = (String) mContinentSpinner.getAdapter().getItem(position);
                loadCountriesByContinent(continent);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mCountryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selectedCountryName = (String) mCountryListView.getAdapter().getItem(position);
                Intent intent = new Intent(CountriesListActivity.this, CountryDetailsActivity.class);
                intent.putExtra("country", selectedCountryName);
                startActivity(intent);

            }
        });
    }
}
