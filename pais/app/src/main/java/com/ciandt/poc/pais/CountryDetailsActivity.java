package com.ciandt.poc.pais;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;

import java.net.URLEncoder;
import java.text.MessageFormat;

public class CountryDetailsActivity extends AppCompatActivity {

    private WebView mDetailsWebView;
    private static final String WIKIPEDIA_URL = "https://en.m.wikipedia.org/wiki/{0}";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_country_details);
            mDetailsWebView = (WebView) findViewById(R.id.details_web_view);
            mDetailsWebView.getSettings().setJavaScriptEnabled(true);
            String country = getIntent().getStringExtra("country");
            country = country.replace(' ', '_');
            String url = MessageFormat.format(WIKIPEDIA_URL, URLEncoder.encode(country,"UTF-8"));
            mDetailsWebView.loadUrl(url);
        } catch (Exception e) {
            Log.e(CountryDetailsActivity.class.getName(), e.getMessage(), e);
        }
    }
}
