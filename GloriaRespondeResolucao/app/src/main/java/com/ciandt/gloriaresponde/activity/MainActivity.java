package com.ciandt.gloriaresponde.activity;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.ciandt.gloriaresponde.R;
import com.ciandt.gloriaresponde.adapter.MessageListAdapter;
import com.ciandt.gloriaresponde.asynctask.ListMessagesAsyncTask;
import com.ciandt.gloriaresponde.asynctask.SenderMessageAsyncTask;
import com.ciandt.gloriaresponde.listener.OnMessageListener;
import com.ciandt.gloriaresponde.model.Message;

import java.util.List;

public class MainActivity extends AppCompatActivity implements OnMessageListener {


    private ListView mLstMessages;
    private EditText mEdtMessage;
    private Button mBtnSend;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mLstMessages = (ListView) findViewById(R.id.lstMessages);
        mEdtMessage = (EditText) findViewById(R.id.txtMensagem);
        mBtnSend = (Button) findViewById(R.id.btnInserir);
        /*TODO 2 - FEITO
         "LINK" os componentes visuais (no xml através do id) com as propriedades desta classe
         usando o método findViewById(id)
         DICAS:
         - Os valores dos ids estão na classe "R" (por exemplo: R.id.lstMessages)
         - Não esqueça de fazer cast do retorno para a classe de componente especificado
         (por exemplo: Button b = (Button)findViewById(R.id.button);)
         */
        asyncLoadData();
        initEvents();
    }

    private void asyncLoadData() {
        if (mProgressDialog != null) {
            if (mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
            mProgressDialog = null;
        }
        mProgressDialog = ProgressDialog.show(this, null, "Carregando", true);
        ListMessagesAsyncTask messagesAsyncTask = new ListMessagesAsyncTask();
        messagesAsyncTask.setOnMessageListener(this);
        messagesAsyncTask.execute();
    }

    private void asyncSendMessage(Message message) {
        if (mBtnSend != null) {
            mBtnSend.setEnabled(false);
        }
        SenderMessageAsyncTask senderMessageAsyncTask = new SenderMessageAsyncTask();
        senderMessageAsyncTask.setOnMessageListener(this);
        senderMessageAsyncTask.execute(message);
    }

    /**
     * Init events
     */
    private void initEvents() {
        /*TODO 3 - FEITO
         Crie um objeto da interface View.OnClickListener para o botão de enviar (mBtnSend:Button)
         e implemente seu método onClick(View v) obtendo o texto da caixa de texto (mEdtMessage:EditView)
         através do método getText() e chamando a função privada sendMessage(String) passando esse texto para ela
         Sete o evento através do método setOnClickListener do componente de Botão (mBtnSend:Button)
         */
        mBtnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = mEdtMessage.getText().toString();
                sendMessage(msg);
            }
        });
    }

    @Override
    public void onLoadMessages(List<Message> messages) {
        if (mLstMessages != null) {
            mLstMessages.setAdapter(new MessageListAdapter(this, messages));
            int count = mLstMessages.getAdapter().getCount() - 1;
            if (count > 0) {
                mLstMessages.setSelection(count);
            }
        }
        if (mProgressDialog != null) {
            if (mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
            mProgressDialog = null;
        }
    }

    @Override
    public void onSendMessage(Message message) {
        if (mLstMessages != null) {
            ((MessageListAdapter) mLstMessages.getAdapter()).add(message);
            int count = mLstMessages.getAdapter().getCount() - 1;
            if (count > 0) {
                mLstMessages.setSelection(count);
            }

        }
        if (mBtnSend != null) {
            mBtnSend.setEnabled(true);
        }
    }

    private void sendMessage(String msg) {
        Message message = new Message();
        message.setUserType(Message.UserType.DEFAULT_USER);
        if ((msg != null) && (!msg.trim().isEmpty())) {
            message.setMessage(msg);
            if (mLstMessages != null) {
                ((MessageListAdapter) mLstMessages.getAdapter()).add(message);
                int count = mLstMessages.getAdapter().getCount() - 1;
                if (count > 0) {
                    mLstMessages.setSelection(count);
                }
            }
            asyncSendMessage(message);
        }
        if (mEdtMessage != null) {
            mEdtMessage.setText("");
        }
    }


}
