package com.ciandt.gloriaresponde.util;

import android.util.Log;

import com.ciandt.gloriaresponde.constants.Constants;
import com.ciandt.gloriaresponde.model.Message;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ricardotakemura on 06/03/16.
 */
public class MessageStreamUtil {

    public static final String GET = "GET";
    public static final String POST = "POST";

    public static void close(Reader reader) {
        try {
            if (reader != null) {
                reader.close();
            }
        } catch (Exception e) {
            Log.e(MessageStreamUtil.class.getName(), e.getMessage(), e);
        }
    }

    public static void close(Writer writer) {
        try {
            if (writer != null) {
                writer.close();
            }
        } catch (Exception e) {
            Log.e(MessageStreamUtil.class.getName(), e.getMessage(), e);
        }
    }

    public static void disconnect(HttpURLConnection connection) {
        if (connection != null) {
            connection.disconnect();
        }
    }

    public static String readFully(InputStream inputStream) {
        BufferedReader reader = null;
        String content = "";
        try {
            reader = new BufferedReader(new InputStreamReader(inputStream));
            String line = null;
            while ((line = reader.readLine()) != null) {
                content += line;
            }
        } catch (Exception e) {
            Log.e(MessageStreamUtil.class.getName(), e.getMessage(), e);
        } finally {
            MessageStreamUtil.close(reader);
        }
        return content;
    }

    public static void printString(OutputStream outputStream, String message) {
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(outputStream);
            writer.print(message);
        } catch (Exception e) {
            Log.e(MessageStreamUtil.class.getName(), e.getMessage(), e);
        } finally {
            MessageStreamUtil.close(writer);
        }
    }

    public static List<Message> callListMessage() {
        List<Message> messages = new ArrayList<>();
        try {
            final URL url = new URL(Constants.HTTP_LIST_MESSAGES_URL);
            final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod(GET);
            conn.setDoInput(true);
            conn.connect();
            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                final String json = readFully(conn.getInputStream());
                messages = Message.fromJSONArray(json);
            }
            conn.disconnect();
        } catch (Exception e) {
            Log.e(MessageStreamUtil.class.getName(), e.getMessage(), e);
        }
        /*TODO 11 - FEITO
         - Crie um objeto da classe List<Message> e instâncie com um construtor vazio
         - Crie e instancie um objeto da classe URL passe para o construtor a constante HTTP_LIST_MESSAGES_URL da classe Constants
         - Use o método openConnection desta classe e receba seu retorno em um objeto da classe HttpURLConnection (faça cast, se necessário)
         - Use o método setRequestMethod e passe o valor "GET" para ele (constante GET desta classe)
         - Use o método setDoInput do objeto HttpURLConnection passando o valor true para ele
         - Use o método connect do objeto HttpURLConnection
         - Obtenha o status da conexão através do método getResponseCode do objeto HttpURLConnection
         - Verifique se seu status é 200 (OK), se sim, faça:
         - Use o método getInputStream deste mesmo objeto e receba seu retorno em um objeto da classe InputStream
         - Use o método readFully desta classe (estático), passe o objeto InputStream e receba seu retorno em um objeto da classe String
         - Chame o método estático fromJSONArray da classe Message passando a String e receba seu retorno em um objeto da classe List<Message>
         - Disconecte a conexão com o método disconnect do objeto HttpURLConnection
         - Retorne o objeto da classe List<Message>
          DICAS:
         - Trate as exceções
         */
        return messages;
    }

    public static Message callSendMessage(Message message) {
        Message ret = new Message();
        try {
            final URL url = new URL(Constants.HTTP_SEND_MESSAGE_URL);
            final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod(POST);
            conn.setDoOutput(true);
            conn.setDoInput(true);
            printString(conn.getOutputStream(), message.toJSONString());
            conn.connect();
            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                final String json = readFully(conn.getInputStream());
                ret = Message.fromJSONObject(json);
            }
            conn.disconnect();
        } catch (Exception e) {
            Log.e(MessageStreamUtil.class.getName(), e.getMessage(), e);
        }
        /*TODO 12 - FEITO
         - Crie um objeto da classe Message e instâncie com um construtor vazio
         - Crie e instancie um objeto da classe URL passe para o construtor a constante HTTP_SEND_MESSAGE_URL da classe Constants
         - Use o método openConnection desta classe e receba seu retorno em um objeto da classe HttpURLConnection (faça cast, se necessário)
         - Use o método setRequestMethod e passe o valor "POST" para ele (constante POST desta classe)
         - Use o método setDoOutput do objeto HttpURLConnection passando o valor true para ele
         - Use o método setDoInput do objeto HttpURLConnection passando o valor true para ele
         - Use o método getOutputStream deste mesmo objeto e receba seu retorno em um objeto da classe OutputStream
         - Use o método printString desta classe (estático) e passe o objeto OutputStream e o retorno do método toJSONString do objeto message (classe Message)
         - Use o método connect do objeto HttpURLConnection
         - Obtenha o status da conexão através do método getResponseCode do objeto HttpURLConnection
         - Verifique se seu status é 200 (OK), se sim, faça:
         - Use o método getInputStream deste mesmo objeto e receba seu retorno em um objeto da classe InputStream
         - Use o método readFully desta classe (estático), passe o objeto InputStream e receba seu retorno em um objeto da classe String
         - Chame o método estático fromJSONObject da classe Message passando a String e receba seu retorno em um objeto da classe Message
         - Disconecte a conexão com o método disconnect do objeto HttpURLConnection
         - Retorne o objeto da classe Message
          DICAS:
         - Trate as exceções
         */
        return ret;
    }
}
