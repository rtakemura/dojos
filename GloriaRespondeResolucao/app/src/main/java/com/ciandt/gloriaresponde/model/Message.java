package com.ciandt.gloriaresponde.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ricardotakemura on 06/03/16.
 */
public class Message implements Serializable {

    public static enum UserType {
        DEFAULT_USER,
        GLORIA
    }

    public static final String USER_TYPE_FIELD_NAME = "user_type";
    public static final String MESSAGE_FIELD_NAME = "message";
    public static final String MESSAGE_DEFAULT = "OCUPADA";

    private UserType userType;
    private String message;

    public Message() {
        userType = UserType.DEFAULT_USER;
        message = "";
    }

    public Message(UserType userType, String message) {
        this.userType = userType;
        this.message = message;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public static List<Message> fromJSONArray(String content) {
        List<Message> messages = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(content);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject item = jsonArray.getJSONObject(i);
                int type = item.getInt(USER_TYPE_FIELD_NAME);
                String message = item.getString(MESSAGE_FIELD_NAME);
                Message msg = new Message(type == 1 ? UserType.GLORIA : UserType.DEFAULT_USER, message);
                messages.add(msg);
            }
        } catch (Exception e) {
            Log.e(Message.class.getName(), e.getMessage(), e);
        }

        /*TODO 10 - FEITO
         - Crie um objeto da classe List<Message> vazio.
         - Parseie o json (array) através do objeto da classe JSONArray, passando em seu construtor o objeto content do tipo String
         - Use uma iteração para percorrer o objeto JSONArray (de 0 até length (método deste objeto). Para cada iteração faça:
         - Use o método getJSONObject passando o indice para obter o contéudo (JSONObject - retorno do método)
         - Use o método getInt do objeto da classe JSONObject, passando o valor da constante USER_TYPE_FIELD_NAME
         para obter o tipo de usuário (0 - DEFAULT_USER, 1 - GLORIA)
         - Transforme o inteiro para o tipo enum UserType
         - Use o método getString do objeto da classe JSONObject, passando o valor da constante MESSAGE_FIELD_NAME
         para obter a mensagem
         - Crie um objeto da classe Message e passe estes dois valores (construtor)
         - Adicione no List<Message> este objeto (método add)
         - Retorne o objeto da classe List<Message>
         DICAS:
         - Trate as exceções
         */
        return messages;

    }

    public static Message fromJSONObject(String content) {
        Message message = new Message();
        try {
            JSONObject jsonObject = new JSONObject(content);
            int type = jsonObject.getInt(USER_TYPE_FIELD_NAME);
            String msg = jsonObject.getString(MESSAGE_FIELD_NAME);
            message = new Message(type == 1 ? UserType.GLORIA : UserType.DEFAULT_USER, msg);

        } catch (Exception e) {
            Log.e(Message.class.getName(), e.getMessage(), e);
        }
        /*TODO 9 - FEITO
         - Parseie o json (objeto) através do objeto da classe JSONObject, passando em seu construtor o objeto content do tipo String
         - Use o método getInt do objeto da classe JSONObject, passando o valor da constante USER_TYPE_FIELD_NAME
         para obter o tipo de usuário (0 - DEFAULT_USER, 1 - GLORIA)
         - Use o método getString do objeto da classe JSONObject, passando o valor da constante MESSAGE_FIELD_NAME
         para obter a mensagem
         - Crie um objeto da classe Message e passe estes dois valores (construtor)
         - Retorne o objeto da classe Message
         DICAS:
         - Trate as exceções
         */
        return message;
    }

    public String toJSONString() {
        /*TODO 8
         - Construa a estrutura JSON na forma de String {"user_type": 0, "message": "Teste"}
         - Retorne a String
         DICAS:
         - Use as constantes USER_TYPE_FIELD_NAME e MESSAGE_FIELD_NAME desta classe para os nomes (chaves) dos campos
         */
        return "{\"" + USER_TYPE_FIELD_NAME + "\":" +
                (UserType.GLORIA.equals(userType) ? 1 : 0) + ",\"" + MESSAGE_FIELD_NAME + "\":\"" + message  + "\"}";
    }
}
